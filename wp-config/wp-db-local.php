<?php
// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

define( 'AUTH_KEY',         '_`}Vf%RH^-6k;rOL}B~Q8]-^/r87Kdsf}=lY+TV%B)bQ4[g9[Y[9m7#e`6G9}]t2' );
define( 'SECURE_AUTH_KEY',  'H)r(2^P}SPdQf>QP&|7c7W1@$zJuR+%u*P~G7^7HpR,?@34GYIb%@+: cD?B/iP_' );
define( 'LOGGED_IN_KEY',    'v >2&*vB5oeE}H F3:R& tawl@WHa{IuUm!96HJyvi~~O[Ttdns*lgA2hoc(]#t|' );
define( 'NONCE_KEY',        '&dg^B5|<`gc [QzrVBJMt4eJ!b%h-4b(OXw:[AT2{1E((=Fk K|D^x0VuV_Dp.xG' );
define( 'AUTH_SALT',        'UtioLWq7vfuQ{B8GoA/i1$cQ:+/%<kk4i`s(bF00gX`Ar$1AA70|gi!NyJw2 *[o' );
define( 'SECURE_AUTH_SALT', 'BZ/`4&_:~5/p<:ifz]Pj>mCWm`{H.;3.()nr#}-Og$`tU793bR?~s,r`s&@c{Km ' );
define( 'LOGGED_IN_SALT',   '_w?Qr?3TwI:wal,V1P]0oB}y`p=c?;IsAt;$f5 @P>nx$%bgh}.US$rl+{]u7T|E' );
define( 'NONCE_SALT',       'YSOID1#L0}myE(`0y=Oy<@&f=8O[J0o+1Vr0pA)j$WoTs!92[8k3JyBa3#v;b!mk' );

define('JWT_AUTH_SECRET_KEY', 'yY[jk[P2JYiSb||S[[>3Zt)%S/Ic#&d5B+!Y4 /)~$XrurxRp_DS9,`2gH(A`|z+');
define('JWT_AUTH_CORS_ENABLE', true);

$table_prefix = 'wp_';

define( 'WP_DEBUG', false );

if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
