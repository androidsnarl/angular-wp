import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HeaderComponent } from './shared/components/header/header.component';
import { FooterComponent } from './shared/components/footer/footer.component';
import { HomeModule } from './modules/home/home.module';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { appRoutes } from './app-routing.module';
import { WprestService } from './shared/wp-rest.service';
import { CF7Service } from './shared/cf7.service';
import { MaterialModule } from './material-module';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes),
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    HomeModule
  ],
  providers: [WprestService, CF7Service],
  bootstrap: [AppComponent],
  exports: [RouterModule]
})
export class AppModule { }
