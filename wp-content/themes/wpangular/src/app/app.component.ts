import { Component } from '@angular/core';
import { WprestService } from './shared/wp-rest.service';

import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { RouterOutlet } from '@angular/router';
import { routeChangeAnimation } from './change-route-animation';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [routeChangeAnimation]
})
export class AppComponent {

  private unsubscribeOnDestroy: Subject<void> = new Subject();

  constructor ( private wprestService: WprestService ) {

  }

  getRouteAnimationState(outlet: RouterOutlet) {
    return (
      outlet &&
      outlet.activatedRouteData &&
      outlet.activatedRouteData['animation']
    )
  }

  ngOnInit(){
    this.checkAndValidateLogin(); 
  }

  // Check if there is a token (from previous JWT Authentication) and see if it is valid
  checkAndValidateLogin(){

    // Check if user token is set in localstorage
    if (localStorage.hasOwnProperty("userToken")){

      //Check it is a valid token
      this.wprestService.validateToken().pipe(takeUntil(this.unsubscribeOnDestroy)).subscribe(
        data=>{
        console.log("Token Validation result:");
        console.log(data);
        },
        error=>{
        console.log("Token Validation Error:");
        console.log(error);
        localStorage.removeItem('userToken');
        });
      
    } else{
      console.log('No token found');
    }
  }

}
