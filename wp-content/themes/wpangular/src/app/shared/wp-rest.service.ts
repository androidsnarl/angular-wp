import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { ConstantGlobals } from './constant-globals';
import { WP_Category } from './models/category';
import { Nav_Link } from './models/nav_link';
import { WP_Page } from './models/page';
import { WP_Post } from './models/post';
import { WP_Slide } from './models/slide';

@Injectable()
export class WprestService {
  slides: Array<WP_Slide>
  contact: Array<WP_Post>
  
  constructor(private http: HttpClient) {
    this.getAllSlides().subscribe((data: Array<WP_Slide> ) => {
      this.slides = data;
    });
    this.getContactPage().subscribe((data: Array<WP_Post> ) => {
      this.contact = data;
    });
  }

  getAllPosts(): Observable<Array<WP_Post>> {
    return this.http.get<Array<WP_Post>>(ConstantGlobals.WP_API_BASE + "/wp/v2/posts");
  }

  getPostBySlug(slug: string): Observable<Array<WP_Post>> {
    return this.http.get<Array<WP_Post>>(ConstantGlobals.WP_API_BASE + "/wp/v2/posts/?slug=" + slug);
  }

  getPageBySlug(slug: string): Observable<Array<WP_Page>> {
    return this.http.get<Array<WP_Page>>(ConstantGlobals.WP_API_BASE + "/wp/v2/pages/?slug=" + slug);
  }

  getAllSlides(): Observable<Array<WP_Slide>> {
    return this.http.get<Array<WP_Slide>>(ConstantGlobals.WP_API_BASE + "/wp/v2/slider");
  }

  getHeaderLinks(): Observable<Nav_Link> {
    return this.http.get<Nav_Link>(ConstantGlobals.WP_API_BASE + "/wpangular/v1/menus/location/main_menu");
  }

  getFooterLinks(): Observable<Nav_Link> {
    return this.http.get<Nav_Link>(ConstantGlobals.WP_API_BASE + "/wpangular/v1/menus/location/footer_menu");
  }

  getContactPage(): Observable<Array<WP_Page>> {
    return this.http.get<Array<WP_Page>>(ConstantGlobals.WP_API_BASE + "/wp/v2/pages/?slug=contact");
  }

  getAllCategories(): Observable<Array<WP_Category>> {
    return this.http.get<Array<WP_Category>>(ConstantGlobals.WP_API_BASE + "/wp/v2/categories");
  }

  getPostsByCategory(id: string): Observable<Array<WP_Post>> {
    return this.http.get<Array<WP_Post>>(ConstantGlobals.WP_API_BASE + "/wp/v2/posts?categories=" + id);
  }

  // LOGIN AND LOGOUT
  
  // Login (currently via the JWT Auth plugin)
  doLogin(userLoginData){
    return this.http.post(ConstantGlobals.WP_API_BASE + '/jwt-auth/v1/token',userLoginData);
  }

  // Validate current user token
  validateToken(){  
    // Pass any empty object (because a request body is required)
    let emptyObject = {}

    // Send POST request
    return this.http.post(ConstantGlobals.WP_API_BASE + '/jwt-auth/v1/token/validate',emptyObject);
  }
}