import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ConstantGlobals } from './constant-globals';
import { CF7_Request, CF7_Response } from './models/CF7_Model';

@Injectable()
export class CF7Service {

  constructor(private http: HttpClient) {

  }

  sendFormEntries(requestModel: CF7_Request, id:string): Observable<CF7_Response> {
    let formData: any = new FormData();

    for (var key in requestModel) {
      formData.append(key, requestModel[key]);
    }
    return this.http.post<CF7_Response>(ConstantGlobals.WP_API_BASE + "/contact-form-7/v1/contact-forms/" + id + "/feedback", formData);
  }

}