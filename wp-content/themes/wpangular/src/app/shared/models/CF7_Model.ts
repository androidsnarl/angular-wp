export class CF7_Request
{
  firstlastname: string;
  email: string;
  region: string;
  gender: string;
  message: string;
}

export class CF7_Response {
  into: string;
  status: string;
  message: string;
}