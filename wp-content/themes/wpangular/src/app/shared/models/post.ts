export class WP_PostContent
{
  rendered: string;
}

export class WP_Post
{
  id: number;
  title: WP_PostContent;
  date: Date;
  content: WP_PostContent;
}