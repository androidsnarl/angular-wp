export class WP_SlideContent
{
  rendered: string;
}

export class WP_Slide
{
  id: number;
  title: WP_SlideContent;
  date: Date;
  content: WP_SlideContent;
}