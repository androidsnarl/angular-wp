export class WP_PageContent
{
  rendered: string;
}

export class WP_Page
{
  id: number;
  title: WP_PageContent;
  date: Date;
  content: WP_PageContent;
}