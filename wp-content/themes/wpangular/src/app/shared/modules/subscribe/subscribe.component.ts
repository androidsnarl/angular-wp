import { Component, OnInit } from '@angular/core';
import { CF7Service } from '../../cf7.service';
import { CF7_Request, CF7_Response } from '../../models/CF7_Model';

@Component({
  selector: 'app-subscribe',
  templateUrl: './subscribe.component.html',
  styleUrls: ['./subscribe.component.scss']
})
export class SubscribeComponent implements OnInit {

  cf7request: CF7_Request;
  form_id = "40";

  constructor( private cf7service:CF7Service ) { 
    this.cf7request = new CF7_Request();
  }

  ngOnInit() {
  }

  getFormEntries() {
    console.log(this.cf7request);

    this.cf7service.sendFormEntries(this.cf7request, this.form_id).subscribe((cf7response: CF7_Response ) => {
      console.log(cf7response);
    });
  }

}
