import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SubscribeComponent } from './subscribe.component';
import { FormsModule } from '@angular/forms';
import { MaterialModule } from 'src/app/material-module';
import { MatFormFieldModule } from '@angular/material/form-field';



@NgModule({
  declarations: [SubscribeComponent],
  imports: [
    CommonModule,
    FormsModule,
    MaterialModule,
    MatFormFieldModule,
  ],
  exports: [SubscribeComponent]
})
export class SubscribeModule { }
