import { Component, OnInit } from '@angular/core';
import { Nav_Link } from '../../models/nav_link';
import { WprestService } from '../../wp-rest.service';


@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  menuList: [];
  constructor(private wprestService: WprestService) { }

  ngOnInit(): void {
    this.wprestService.getFooterLinks().subscribe((result: Nav_Link) => {
      this.menuList = result.items;
    });
  }
  
}
