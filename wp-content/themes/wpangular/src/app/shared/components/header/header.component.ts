import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Nav_Link } from '../../models/nav_link';
import { WprestService } from '../../wp-rest.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  private unsubscribeOnDestroy: Subject<void> = new Subject();

  menuList: [];

  userToken = localStorage.getItem('userToken');
  username; // user
  password; // admin
  
  constructor( private wprestService: WprestService, private router: Router ){}

  ngOnInit(): void {
    this.wprestService.getHeaderLinks().subscribe((result: Nav_Link) => {
      this.menuList = result.items;
    });
  }

  doLogin(){
    let userLoginData = {
      username: this.username,
      password: this.password
    }
    
    this.wprestService.doLogin(userLoginData).pipe(takeUntil(this.unsubscribeOnDestroy)).subscribe(
      data=>{
        localStorage.setItem('userToken', data['token']);
        console.log(userLoginData);
        // Redirect to home
        this.router.navigate(['']);
      },
      error=>{
        console.log(error['error'].code);
      }
    );
  }

}
