export const ConstantGlobals = Object.freeze({
    /*
    - WP_ROOT_URL is the root url of your WordPress installation
      (this is your public WordPress) 
    - WP_API_BASE is the base URL for your WordPress API
      (you can use a different URL here if you want a separate backend WP)
    */
    WP_ROOT_URL: 'http://angular-wp.herokuapp.com/',         // No trailing slash
    WP_API_BASE: 'http://angular-wp.herokuapp.com/wp-json'  // No trailing slash
});