import { Routes } from '@angular/router';
import { HomeComponent } from './modules/home/home.component';

export const appRoutes: Routes = [
  {
    path: '', component: HomeComponent
  },
  {
    path: 'blog',
    loadChildren: ()=> import ('./modules/blog/blog.module').then(m=>m.BlogModule)
  },
  {
    path: 'contact',
    loadChildren: ()=> import ('./modules/contact/contact.module').then(m=>m.ContactModule)
  },
  {
    path: 'products',
    loadChildren: ()=> import ('./modules/products/products.module').then(m=>m.ProductsModule)
  },
  { 
    path: 'post/:slug', 
    loadChildren: ()=> import("./modules/post/post.module").then(m=>m.PostModule) 
  },
  { 
    path: 'page/:slug', 
    loadChildren: ()=> import("./modules/page/page.module").then(m=>m.PageModule) 
  },
  {
    path: '**',
    redirectTo: ''
  }
];
