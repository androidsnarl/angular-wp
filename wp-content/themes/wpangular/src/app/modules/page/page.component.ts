import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { WP_Page } from 'src/app/shared/models/page';
import { WprestService } from 'src/app/shared/wp-rest.service';

@Component({
  selector: 'app-page',
  templateUrl: './page.component.html',
  styleUrls: ['./page.component.scss']
})
export class PageComponent implements OnInit {

  wprestService: WprestService;
  slug: string;
  items: Array<WP_Page>;
  item: WP_Page;

  constructor(wprestService: WprestService, private route: ActivatedRoute) {
    this.wprestService = wprestService;
  }

  ngOnInit(): void {
    this.slug = String(this.route.snapshot.paramMap.get('slug'));
    this.wprestService.getPageBySlug(this.slug).subscribe((data: Array<WP_Page> ) => {
      this.items = data;
      this.item = this.items[0];
    });
  }

}
