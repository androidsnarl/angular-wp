import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageRoutingModule } from './page-routing.module';
import { PageComponent } from './page.component';
import { HeroModule } from 'src/app/shared/modules/hero/hero.module';



@NgModule({
  declarations: [PageComponent],
  imports: [
    CommonModule,
    PageRoutingModule, 
    HeroModule
  ]
})
export class PageModule { }
