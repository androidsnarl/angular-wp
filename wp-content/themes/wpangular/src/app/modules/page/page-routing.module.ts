import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PageComponent } from './page.component';

const pageRoutes: Routes =[
    { path: '', component: PageComponent, data: {animation: 'Page'}},
];

@NgModule({
    imports:    [ RouterModule.forChild(pageRoutes)],
    exports:    [ RouterModule]
})
export class PageRoutingModule { }