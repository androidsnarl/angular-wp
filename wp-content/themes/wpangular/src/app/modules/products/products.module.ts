import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { productsRoutes } from './products-routing.module';
import { RouterModule } from '@angular/router';
import { ProductsComponent } from './products.component';



@NgModule({
  declarations: [ProductsComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(productsRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class ProductsModule { }
