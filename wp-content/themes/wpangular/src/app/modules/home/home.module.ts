import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from 'src/app/material-module';
import { MatCarouselModule } from '@ngmodule/material-carousel';
import { HomeSliderComponent } from './home-slider/home-slider.component';
import { HomeBlogComponent } from './home-blog/home-blog.component';
import { HomeComponent } from './home.component';
import { RouterModule } from '@angular/router';
import { homeRoutes } from './home-routing.module';
import { SubscribeModule } from 'src/app/shared/modules/subscribe/subscribe.module';



@NgModule({
  declarations: [HomeSliderComponent, HomeBlogComponent, HomeComponent],
  imports: [
    CommonModule,
    SubscribeModule,
    MaterialModule,
    MatCarouselModule.forRoot(),
    RouterModule.forChild(homeRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class HomeModule { }
