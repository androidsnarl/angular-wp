import { Component, OnInit } from '@angular/core';
import { WP_Post } from 'src/app/shared/models/post';
import { WprestService } from 'src/app/shared/wp-rest.service';

@Component({
  selector: 'app-home-blog',
  templateUrl: './home-blog.component.html',
  styleUrls: ['./home-blog.component.scss']
})
export class HomeBlogComponent implements OnInit {

  postList: Array<WP_Post>;
  constructor( private wprestService: WprestService ){}

  ngOnInit(): void {
    this.wprestService.getAllPosts().subscribe((data: Array<WP_Post>) => {
      this.postList = data.slice(0,4);
    });
  }
}
