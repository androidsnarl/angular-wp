import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { WP_Post } from 'src/app/shared/models/post';
import { WprestService } from 'src/app/shared/wp-rest.service';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.scss']
})
export class PostComponent implements OnInit {

  wprestService: WprestService;
  slug: string;
  items: Array<WP_Post>;
  item: WP_Post;

  constructor(wprestService: WprestService, private route: ActivatedRoute) {
    this.wprestService = wprestService;
  }

  ngOnInit(): void {
    this.slug = String(this.route.snapshot.paramMap.get('slug'));
    console.log(this.slug);
    this.wprestService.getPostBySlug(this.slug).subscribe((data: Array<WP_Post> ) => {
      this.items = data;
      this.item = this.items[0];
      console.log(this.item);
    });
  }

}
