import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CF7Service } from 'src/app/shared/cf7.service';
import { CF7_Request, CF7_Response } from 'src/app/shared/models/CF7_Model';
import { WP_Page } from 'src/app/shared/models/page';
import { WP_Post } from 'src/app/shared/models/post';
import { WprestService } from 'src/app/shared/wp-rest.service';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {

  item: WP_Post;
  cf7request: CF7_Request;
  form_id = "6";

  constructor(private wprestService: WprestService, private cf7service:CF7Service,  private route: ActivatedRoute) { 
    this.cf7request = new CF7_Request();
  }

  ngOnInit() {
    this.item = this.wprestService.contact[0];
  }

  getFormEntries() {
    console.log(this.cf7request);

    this.cf7service.sendFormEntries(this.cf7request, this.form_id).subscribe((cf7response: CF7_Response ) => {
      console.log(cf7response);
    });
  }

}
