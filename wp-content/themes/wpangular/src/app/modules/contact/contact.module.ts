import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { contactRoutes } from './contact-routing.module';
import { RouterModule } from '@angular/router';
import { ContactComponent } from './contact.component';
import { HeroModule } from 'src/app/shared/modules/hero/hero.module';
import { MatFormFieldModule } from '@angular/material/form-field';
import { FormsModule } from '@angular/forms';
import { MaterialModule } from 'src/app/material-module';



@NgModule({
  declarations: [ContactComponent],
  imports: [
    CommonModule,
    FormsModule,
    MaterialModule,
    HeroModule,
    MatFormFieldModule,
    RouterModule.forChild(contactRoutes),
  ],
  exports: [
    RouterModule
  ]
})
export class ContactModule { }
