import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { blogRoutes } from './blog-routing.module';
import { RouterModule } from '@angular/router';
import { BlogComponent } from './blog.component';
import { MaterialModule } from 'src/app/material-module';



@NgModule({
  declarations: [BlogComponent],
  imports: [
    CommonModule,
    MaterialModule,
    RouterModule.forChild(blogRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class BlogModule { }
