import { Component, HostListener, OnInit } from '@angular/core';
import { WP_Category } from 'src/app/shared/models/category';
import { WP_Post } from 'src/app/shared/models/post';
import { WprestService } from 'src/app/shared/wp-rest.service';

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.scss']
})
export class BlogComponent implements OnInit {

  postList: Array<WP_Post>;
  categoryList:Array<WP_Category>

  value: string;

  constructor( private wprestService: WprestService ) { }

  ngOnInit() {
    this.wprestService.getAllPosts().subscribe((data: Array<WP_Post>) => {
      this.postList = data;
    });
    this.wprestService.getAllCategories().subscribe((data: Array<WP_Category>) => {
      this.categoryList = data;
    });
  }

  public getInputValue() {
    //TODO: Сделай inputValue не локальной переменной, а проперти блог компонента
    //и используй привязку по атрибуту:  <input [value]='inputValue' />
    var inputValue = ((document.getElementById("search") as HTMLInputElement).value);
    console.log(inputValue);


    this.wprestService.getAllPosts().subscribe((data: Array<WP_Post>) => {
      const filteredItem = data.filter(item => {
        return item.title.rendered.includes(inputValue) || item.content.rendered.includes(inputValue);
      });

      this.postList = filteredItem;
    });
  }

  // @HostListener("click", ["$event"])
  // @HostListener("click", ["$event"])
  public onClick(catId: string): void {

    //call API and update the state of the host component
    this.wprestService.getPostsByCategory(catId).subscribe((data: Array<WP_Post>) => {
      this.postList = data;
    });

  }

}
